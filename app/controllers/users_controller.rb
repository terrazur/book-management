class UsersController < ApplicationController
	skip_before_action :authorize_request, only: :create
  before_action :set_user, only: [:show, :edit_account, :edit_password, :edit_role]

  def create
    params[:role] = 2
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  def show
    json_response(@user.slice(:name, :email, :phone, :role))  
  end

  def edit_account
    @user.update!(user_params_edit_account)
    response = { messages: "Account successfully updated" }
    json_response(response)
  end

  def edit_password
    @user.update!(user_params_edit_password)
    response = { messages: "Password successfully updated" }
    json_response(response)
  end

  def edit_role
    if @user.role == 1
      @admin = @user
      @user = User.find(params[:id])
      if @user.id == @admin.id
        response = { messages: "Why...." }
        json_response(response)
      else
        @user.update_attribute(:role, params[:role])
        response = { messages: "Role successfully updated" }
        json_response(response)
      end
    else
      response = { messages: "You are unauthorized to do the action" }
      json_response(response)
    end
  end

  private

  def user_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation,
      :phone,
      :role,
    )
  end

  def user_params_edit_account
    params.permit(
      :name,
      :email,
      :phone,
    )
  end

  def user_params_edit_password
    params.permit(
      :password,
      :password_confirmation,
    )
  end

  def set_user
    @user = current_user
  end
end