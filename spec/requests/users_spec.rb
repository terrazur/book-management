require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  describe 'POST /api/register' do
    let(:user) { build(:user) }
    let(:headers) { valid_headers.except('Authorization') }
    let(:valid_attributes) do
      attributes_for(:user, password_confirmation: user.password)
    end
    context 'when valid request' do
      before { post '/api/register', params: valid_attributes.to_json, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(201)
      end

      it 'returns success message' do
        expect(json['message']).to match(/Account created successfully/)
      end

      it 'returns an authentication token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'when invalid request' do
      before { post '/api/register', params: {}, headers: headers }

      it 'does not create a new user' do
        expect(response).to have_http_status(422)
      end

      it 'returns failure message' do
        expect(json['message'])
          .to match(/Validation failed: Password can't be blank, Name can't be blank, Email can't be blank, Password digest can't be blank, Phone can't be blank/)
      end
    end
  end

  describe 'GET /api/show/account' do
    let(:user) {create(:user)}
    let(:headers2) { valid_headers }
    context 'when user exists' do
      before { get "/api/show/account", params: {}, headers: headers2 }

      it 'returns user data' do
        expect(json['name']).to eq(user.name)
      end
    end

    context 'when user does not exist' do
      before { get "/api/show/account", params: {}, headers: headers }

      it 'returns error' do
        expect(json['message'])
          .to match(/Missing token/)
      end
    end
  end

  describe 'PATCH /api/edit/account/password' do
    let(:user) {create(:user)}
    let(:headers2) { valid_headers }
    let(:valid_credentials) do
      {
        password: "ultar",
        password_confirmation: "ultar"
      }.to_json
    end

    let(:invalid_credentials) do
      {
        password: "ultar",
        password_confirmation: "altar"
      }.to_json
    end


    context 'when valid request' do
      before { patch "/api/edit/account/password", params: valid_credentials, headers: headers2 }
      it 'updated successfully' do
        expect(json.size).to eq(1)
        expect(json['messages']).to eq('Password successfully updated');
      end
    end

    context 'when user does not exist' do
      before { patch "/api/edit/account/password", params: valid_credentials, headers: {} }

      it 'returns error' do
        expect(json['message'])
          .to match(/Missing token/)
      end
    end
  end

  describe 'PATCH /api/edit/account' do
    let(:user) {create(:user)}
    let(:headers2) { valid_headers }
    let(:valid_credentials) do
      {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        phone: Faker::PhoneNumber.phone_number,
      }.to_json
    end

    context 'when valid request' do
      before { patch "/api/edit/account", params: valid_credentials, headers: headers2 }
      it 'updated successfully' do
        expect(json.size).to eq(1)
        expect(json['messages']).to eq('Account successfully updated');
      end
    end

    context 'when user does not exist' do
      before { patch "/api/edit/account", params: valid_credentials, headers: {} }

      it 'returns error' do
        expect(json['message'])
          .to match(/Missing token/)
      end
    end
  end

  # describe 'PATCH api/edit/account/:id/role' do
  #   let(:admin) {create(:user)}
  #   let(:user) {create(:user)}
  #   let(:user_id) {user.id}
  #   let(:headers2) { valid_headers }
    

  #   context 'when valid request' do
  #     let(:valid_attributes) do
  #       attributes_for(:user, role: 1)
  #     end
  #     before { patch "api/edit/account/#{user_id}/role", params: valid_attributes.to_json, headers: headers2 }
  #     it 'updated successfully' do
  #       expect(json.size).to eq(1)
  #       expect(json['messages']).to eq('Account successfully updated');
  #     end
  #   end

  #   context 'when user does not exist' do
  #     before { patch "api/edit/account/{}/role", params: valid_credentials, headers: {} }

  #     it 'returns error' do
  #       expect(json['message'])
  #         .to match(/Missing token/)
  #     end
  #   end
  # end
end