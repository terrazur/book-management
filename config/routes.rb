Rails.application.routes.draw do

	get 'api/show/account', to: 'users#show'
	patch 'api/edit/account', to: 'users#edit_account'
	patch 'api/edit/account/password', to: 'users#edit_password'
	patch 'api/edit/account/:id/role', to: 'users#edit_role'

  post 'api/login', to: 'authentication#authenticate'
  post 'api/register', to: 'users#create'
end
